﻿using UnityEngine;

public class EnemyMovement
{
    public bool IsMovment { get; set; }
    private Transform enemyTransform;
    private Transform rayPoint;
    private int movingSpeed;
    private int movingDistance;
    private int coificentForRotate = 30;

    public EnemyMovement(Transform enemyTransform, Transform rayPoint, int movingSpeed, int movingDistance)
    {
        this.enemyTransform = enemyTransform;
        this.movingSpeed = movingSpeed;
        this.movingDistance = movingDistance;
        this.rayPoint = rayPoint;
    }

    //moves the enemy to the given point return true when reached
    public bool MoveToPositio(Vector3 toPosition)
    {
        toPosition.y = enemyTransform.position.y;
        if (enemyTransform.position != toPosition)
        {
            enemyTransform.position = Vector3.MoveTowards(enemyTransform.position, toPosition, Time.deltaTime * 3f);
            return false;
        }
        return true;
    }
    //rotate enemy to the given point return true when reached
    public bool RotateToPosition(Vector3 rotateToPos)
    {
        rotateToPos.y = enemyTransform.position.y;
        Quaternion lookOn = Quaternion.LookRotation(rotateToPos - enemyTransform.position);
        if (lookOn != enemyTransform.rotation)
        {
            enemyTransform.rotation = Quaternion.RotateTowards(enemyTransform.rotation, lookOn, Time.deltaTime * movingSpeed * coificentForRotate);
            return false;
        }
        else
            return true;
    }

    // get new position to move the enemy 
    public Vector3 GetNewMovePosition(Transform player)
    {
        RaycastHit hit;
        Vector3 newpos;
        Vector3 direVector = player.position - enemyTransform.position;
        direVector = direVector.normalized;
        Physics.Raycast(rayPoint.position, direVector, out hit);
        if (hit.transform.tag == player.tag)//if see player
        {
            float dis = Vector3.Distance(player.position, enemyTransform.position);
            if (movingDistance > dis)
                newpos = new Vector3(hit.point.x, enemyTransform.position.y, hit.point.z) - direVector * 5f;
            else
                newpos = enemyTransform.position + (direVector * movingDistance);
        }
        else newpos = NewPositionCantSee(movingDistance);
        return newpos;
    }

    //generate position when enemy can't see the player
    private Vector3 NewPositionCantSee(float moveDistanc)
    {
        Vector3 newPos = GetRandomPosition(enemyTransform.position, moveDistanc);
        Vector3 direVector = newPos - enemyTransform.position;
        direVector = direVector.normalized;
        RaycastHit hit;
        Physics.Raycast(rayPoint.position, direVector, out hit);
        float dis = Vector3.Distance(newPos, enemyTransform.position);
        float hitdis = hit.distance;
        if (dis < hitdis)
            return newPos;
        else
            return hit.point - (direVector * 5);
    }

    //generate new random position from the set position  in a set max distance 
    private Vector3 GetRandomPosition(Vector3 fromPosition, float maxDistance)
    {
        float x, z;
        x = Random.Range(-1 * maxDistance, maxDistance);
        z = maxDistance - Mathf.Abs(x);
        if (Random.Range(-1f, 1f) > 0) z *= -1;
        return new Vector3(enemyTransform.position.x + x, enemyTransform.position.y, enemyTransform.position.z + z);
    }

    // set enemy y pos
    public void SetYpos(float yPos)
    {
        enemyTransform.position = new Vector3(enemyTransform.position.x, yPos, enemyTransform.position.z);
    }
}
