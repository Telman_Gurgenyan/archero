﻿using System.Collections;
using UnityEngine;

public class EnemyShootingManeger : MonoBehaviour
{
    public Transform shotPoint;
    public float shootingFrequency = 0.3f;
    public GameObject bulletObj;
    private EnemyControl enemyControl;
    private Bullet bullet;

    // Start is called before the first frame update
    void Start()
    {
        enemyControl = transform.GetComponent<EnemyControl>();
        StartCoroutine(Shoot());
    }

    // taimer Shoot
    IEnumerator Shoot()
    {
        while (true)
        {
            if (!enemyControl.IsMovement)
            {
                bullet = bulletObj.transform.GetComponent<Bullet>();
                bullet.targeBullet = transform.GetComponent<Target>().target;
                bullet.demageBullet = transform.GetComponent<HealthManeger>().demage;
                Instantiate(bulletObj, shotPoint.position, shotPoint.rotation);
            }
            yield return new WaitForSeconds(shootingFrequency);
        }
    }
}
