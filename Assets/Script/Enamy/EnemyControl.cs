﻿using System.Collections;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
    public bool IsMovement { get; set; }
    public int movingSpeed = 5;
    public int movingDistance = 5;
    public float idleTime = 5;
    private Transform playerTransform;
    private Transform enemyTransform;
    private EnemyMovement enemyMovement;
    private Vector3 newPosition;
    private RaycastHit hit;
    private Transform rayPoint;
    private bool movetoPosition = true;
    private bool isRotatetoplayer = false;
    private bool isRotateToPos = true;


    //waiting when will end idle time 
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(idleTime);
        StopCoroutine(Timer());
        newPosition = enemyMovement.GetNewMovePosition(playerTransform);
        IsMovement = true;
    }

   // set all settings for controlling mast call child class start
    protected void SetSetting(Transform enemyTransform, float heightMovement)
    {
        this.enemyTransform = enemyTransform;
        string target = enemyTransform.GetComponent<Target>().target;
        playerTransform = GameObject.FindGameObjectWithTag(target).transform;
        rayPoint = enemyTransform.Find("RayPoint").transform;
        enemyMovement = new EnemyMovement(enemyTransform, rayPoint, movingSpeed, movingDistance);
        enemyMovement.SetYpos(heightMovement);
        newPosition = enemyMovement.GetNewMovePosition(playerTransform);
        IsMovement = true;
    }

    //control call in child  class update
    protected void MovmentControl()
    {
        if (IsMovement)
        {
            if (isRotateToPos)
            {
                if (enemyMovement.RotateToPosition(newPosition))
                {
                    isRotateToPos = false;
                    movetoPosition = true;
                }
            }
            else if (movetoPosition)
            {
                if (enemyMovement.MoveToPositio(newPosition))
                {
                    movetoPosition = false;
                    isRotatetoplayer = true;
                }
            }
            else if (isRotatetoplayer)
            {
                if (enemyMovement.RotateToPosition(playerTransform.position))
                {
                    isRotatetoplayer = false;
                    isRotateToPos = true;
                    IsMovement = false;
                    StartCoroutine(Timer());
                }
            }
        }
        else enemyTransform.LookAt(playerTransform.position);
    }
}



