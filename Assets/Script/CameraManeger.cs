﻿using UnityEngine;

public class CameraManeger : MonoBehaviour
{
    public Transform player;
    private Vector3 dir;
    private float widthDef = 16;
    private float heightDef = 9;
    private float asceptDef;
    private float asceptscreen;
    // Start is called before the first frame update
    void Start()
    {
        SetCameraYpos(transform);
        dir = transform.position - player.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = player.position;
        pos.x = 0;
        transform.position  = Vector3.Lerp(transform.position,  pos + dir, Time.deltaTime * 3);//following player
    }
  // set size camera dipending screen 
    private void SetCameraYpos(Transform transform)
    {
        asceptDef = widthDef / heightDef;
        asceptscreen = (float)Screen.width / Screen.height;
        float coif = asceptDef / transform.position.y;
        float distancY = (asceptDef - asceptscreen) / coif;
        transform.position = new Vector3(transform.position.x, transform.position.y + distancY, transform.position.z);
    }
}
