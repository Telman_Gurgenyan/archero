﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManeger : MonoBehaviour
{
    private GameControl gameControl;
    public GameObject playButton;
    public GameObject pauseButton;
    public GameObject winPanel;
    public GameObject lostPnel;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
        gameControl.playerLostEvent += PLayerLost;
        gameControl.playerWinEvent += PlayerWin;
    }

    void PLayerLost()
    {
        Time.timeScale = 0f;
        pauseButton.SetActive(false);
        lostPnel.SetActive(true);
    }

    void PlayerWin()
    {
        Time.timeScale = 0f;
        pauseButton.SetActive(false);
        winPanel.SetActive(true);
    }

    public void Play()
    {
        pauseButton.SetActive(true);
        playButton.SetActive(false);
        Time.timeScale = 1f;  
    }

    public void Pause()
    {
        playButton.SetActive(true);
        Time.timeScale = 0f;
        pauseButton.SetActive(false);
    }

    public void Again()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnDisable()
    {
        gameControl.playerLostEvent -= PLayerLost;
        gameControl.playerWinEvent -= PlayerWin;
    }

}
