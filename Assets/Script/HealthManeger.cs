﻿using UnityEngine;

public class HealthManeger : MonoBehaviour
{
    public int health = 10;
    public int demage = 2;
    private GameControl gameControl;
    private void Start()
    {
        gameControl = GameObject.Find("GameControl").GetComponent<GameControl>();
    }

    //checks health and kills if it is not there, call events
    public void IsDemage(int setDemage)
    {
        if (setDemage > health)
        {
            if (transform.tag == StaticValues.playerTag)
                gameControl.PlayerWasKilled();
            else
                gameControl.EnemyWasKilled();

            Destroy(transform.gameObject);
        }
        else
            health -= setDemage;
    }
}
