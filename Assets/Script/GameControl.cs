﻿using UnityEngine;

public class GameControl : MonoBehaviour
{
    public delegate void GameEvent();
    public event GameEvent playerWinEvent;
    public event GameEvent playerLostEvent;
    private int enemyCount;
    // Start is called before the first frame update
    void Start()
    {
        enemyCount = GameObject.FindGameObjectsWithTag(StaticValues.enemyTag).Length;
    }

    //call event when player wos killed
    public void PlayerWasKilled()
    {
        playerLostEvent();
    }

    //call event when enemy wos killed
    public void EnemyWasKilled()
    {
        if (enemyCount > 1)
            enemyCount--;
        else
            playerWinEvent();
    }
}
