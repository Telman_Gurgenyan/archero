﻿using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    private Rigidbody rigidbodyPlayer;
    private Quaternion quaternion;
    public float speed = 3;
    
    // Start is called before the first frame update
    void Start()
    {
        rigidbodyPlayer = GetComponent<Rigidbody>();  
    }

    void FixedUpdate()
    {
        float verticalValue = Input.GetAxisRaw("Vertical") * speed;
        float horizontalValue = Input.GetAxis("Horizontal") * speed;
        quaternion = Quaternion.Euler(0, horizontalValue, 0);
        rigidbodyPlayer.velocity = transform.forward * verticalValue;
        rigidbodyPlayer.rotation = rigidbodyPlayer.rotation * quaternion;
    }
}
