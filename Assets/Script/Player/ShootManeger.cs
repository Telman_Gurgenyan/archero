﻿using System.Collections;
using UnityEngine;

public class ShootManeger : MonoBehaviour
{
    public Transform pointShoot;
    public float shootingFrequency = 1f;
    public GameObject bulletObj;
    private Bullet bullet;

    // Start is called before the first frame update
    private void Start()
    {      
        StartCoroutine(Shoot());
    }
    // shoot timer
    IEnumerator Shoot()
    {
        while (true)
        {
            if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
            {
                bullet = bulletObj.transform.GetComponent<Bullet>();
                bullet.targeBullet = transform.GetComponent<Target>().target;
                bullet.demageBullet = transform.GetComponent<HealthManeger>().demage;
                Instantiate(bulletObj, pointShoot.position, pointShoot.rotation);
            }
            yield return new WaitForSeconds(shootingFrequency);
        }
    }

}
