﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float speed = 10f;
    [HideInInspector]
    public int demageBullet;
    [HideInInspector]
    public string targeBullet;

    // Update is called once per frame
    void Update()
    {
        transform.localPosition += transform.forward * Time.deltaTime * speed;
    }

    private void OnTriggerEnter(Collider other)
    {       
        if (other.tag == targeBullet)
        {
            other.GetComponent<HealthManeger>().IsDemage(2);
            Destroy(transform.gameObject);
        }
        else if (other.tag != StaticValues.bulletTag)
            Destroy(transform.gameObject);
    }
}
